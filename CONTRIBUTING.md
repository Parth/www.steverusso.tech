
## Contributing

Improvements can be made by submitting issues and merge requests to this repository.

Please ensure the following before submission.
* The linter is quiet (run: `yarn lint`)
* If there are dependency changes, ensure the project can be built from scratch:
    ```
    yarn clean
    yarn cache clean
    rm -rf node_modules
    yarn && yarn dev
    ```

---

For more technical details about how to get started with development, please see the [README](https://gitlab.com/steverussotech/steverusso.tech/blob/master/README.md).

For more information about the project in general, including code documentation, please see the [wiki](https://gitlab.com/steverussotech/steverusso.tech/wikis/home).

