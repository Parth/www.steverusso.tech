
# Steve Russo Technologies

Front-end development for [www.steverusso.tech](http://www.steverusso.tech)

## Development

1. Clone the repo.
2. Start the hot reload development server:
   ```shell
   yarn dev
   ```

## Code Style

This project uses `eslint` to enforce code formatting rules (stored in `.eslintrc`).
The linter will ignore the patterns specified in `.eslintignore`.
To execute the linter, run:

```shell
yarn lint
```

## Production

To generate a production build:

```shell
yarn build
```

The resources will be stored in `dist`.

## Analyzing

The `webpack-bundle-analyzer` is an optional dependency.
To view the webpack analyzer on a production build, run:
```shell
yarn analyze
```
