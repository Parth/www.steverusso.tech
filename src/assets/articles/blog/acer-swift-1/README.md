# M.2 SSD Expansion for Acer SF114-32-P2PK

---

I was recently in the market for a budget laptop.
After almost four solid years of using an Acer Chromebook 14, I decided to upgrade to an Acer Swift 1 (the <a v-external href="https://www.acer.com/ac/en/US/content/model/NX.GXGAA.002">SF114-32-P2PK</a> model).

The look and feel of the laptop is exactly what I wanted (things like the aluminum chassis and short key travel time).
However, I ran into some trouble after I installed Linux over the default Windows installation: the BIOS was unable to load.
Also, it turns out that you can only reflash the BIOS while running Windows (???).

I can't say for sure why that is.
What I can say for sure is that *Acer support will not help you after they learn you wiped the default Windows installation.*
(I think because they know that means their device is most likely bricked.)

Luckily, Newegg allowed me to return the bricked laptop for a replacement.
With the new laptop, I needed to find a way to keep the default Windows installation, but also install Linux.
Dual booting might have been a valid option, but I didn't even want to touch the default storage device that has Windows on it.

One cool thing about this model is that it has an M.2 expansion slot (seen below, circled in red):

<app-img data-src="/img/photos/acer-swift-1/overview-ssd-slot-circled.jpg" alt="M.2 slot on Acer SF114-32-P2PK"></app-img>

I saw conflicting reports about exactly which type of drive is supported.
Some forum posts said only M.2 SATA SSDs would work.
Some said that both SATA and NVMe PCIe SSDs would work, but that any NVMe drive *must* have a capacity of 256GB.

So I decided to try both a 256GB NVMe SSD and a 128GB SATA SSD.
The NVMe unfortunately was not recongized.
However, the <a v-external href="https://amzn.to/2WldNCN">ADATA 128GB SATA SSD</a> worked perfectly.
Here is what it looks like installed:

<app-img data-src="/img/photos/acer-swift-1/installed-ssd-circled.jpg" alt="M.2 SATA SSD on Acer SF114-32-P2PK"></app-img>

**Note:** There was no screw that came with the laptop.
(I grabbed the one in the photo from my chromebook.)
However, <a v-external href="https://community.acer.com/en/discussion/comment/598673/#Comment_598673">this forum discussion</a> talks about what type of screw it is and how to potentially find one if it is not included with the SSD purchase.

---

## Conclusion

I was able to install Manjaro on the SSD, and it has been running very well ever since.
The performance difference between the M.2 SSD and the eMMC SSD is quite noticeable at times.
The expanded capacity (from 64GB to 128GB) is very nice to have as well.

Most importantly, I always have the default Windows installation to fall back on in case I need to reflash the BIOS.

<script>
export default {
	head: {
		title: 'M.2 SSD Expansion for Acer SF114-32-P2PK',
		meta: [
			{ hid: 'description', name: 'description', content: 'M.2 SSD Expansion for Acer SF114-32-P2PK' },
			{ hid: 'keywords', name: 'keywords', content: 'acer,swift,1,m.2,ssd,expansion,sf114-32-p2pk' },
		],
	},
}
</script>
