# Building ZFS on a Pine64 Rock64 Ubuntu Distro

---

This is a guide for building and installing ZoL (ZFS on Linux) on a Rock64 Ubuntu distribution, specifically "[Ubuntu 18.04 Bionic minimal](http://wiki.pine64.org/index.php/ROCK64_Software_Release#Ubuntu_18.04_Bionic_minimal_64bit_Image_.5BmicroSD_.2F_eMMC_Boot.5D_.5B0.7.8.5D)."
Most of these instructions are taken from [this Armbian forum comment](https://forum.armbian.com/topic/6789-build-zfs-on-rk3328/?tab=comments#comment-53681).
However, I hit a snag or two that those directions did not cover.

## Prep Work

To kick things off, let's make sure our system packages are upgraded.
Then, install packages `spl-dkms` and `zfs-dkms`.
Note that the second command will fail to install each package.
That is expected.
```bash
sudo apt update && sudo apt upgrade
sudo apt install spl-dkms zfs-dkms
```

Now, go to the `linux-headers` directory (in my case, `/usr/src/linux-headers-4.4.132-1072-rockchip-ayufan-ga1d27dba5a2e`):
```bash
cd /usr/src/linux-headers-4.4.132-1072-rockchip-ayufan-ga1d27dba5a2e
```

From this directory, we are going to do a few things.
First, run:
```bash
sudo make scripts
```
This failed twice for me, both times because of missing header files.

The first time this failed, it was an error that said a header file `classmap.h` was missing.
This was solved by editing `scripts/Makefile` and commenting out the line with 'selinux' in it:
```makefile
# subdir-$(CONFIG_SECURITY_SELINUX) += selinux
```

The second time this failed, it was an error that said `tools/be_byteshift.h` was missing.
This was solved by following the instructions below, which where gathered from the discussion on [this GitHub issue](https://github.com/armbian/build/issues/74).
We first get [the patch file](https://github.com/armbian/build/blob/next/patch/headers-debian-byteshift.patch) and then use it:
```bash
sudo wget https://raw.githubusercontent.com/armbian/build/next/patch/headers-debian-byteshift.patch
sudo patch -p1 < headers-debian-byteshift.patch
```

Next, after successfully running `make scripts`, view the contents of `include/generated/utsrelease.h`.
We need to verifiy that the value of `UTS_RELEASE` matches the output of `uname -r`:
```bash
cat include/generated/utsrelease.h
uname -r
```
If they do not match, edit that file as root and make it so that they do.

## Modifying SPL Configuration

Next, edit `/var/lib/dkms/spl/[version]/source/configure` and search for the following lines:
```cpp
kuid_t userid = KUIDGT_INIT(0);
kgid_t groupid = KGIDT_INIT(0);
```
There are **two sets** of these lines. Delete them both.

Then, search for the following lines:
```cpp
kuid_t userid = 0;
kgid_t groupid = 0;
```
There are **two sets** of these lines. Change them both to:
```cpp
kuid_t userid;
kgid_t groupid;
```

## Building and Installing

Now that all the prep work is done, we can finally build and install the SPL and ZFS modules.
```bash
sudo dkms build spl/[version]
sudo dkms install --force spl/[version]
sudo modprobe spl

sudo dkms build zfs/[version]
sudo dkms install --force zfs/[version]
sudo modprobe zfs
```

Finally, to actually use ZFS, we can install the `zfsutils-linux` package:
```bash
sudo apt install zfsutils-linux
```

<script>
export default {
	head: {
		title: 'Building ZFS on a Pine64 Rock64 Ubuntu Distro',
		meta: [
			{ hid: 'description', name: 'description', content: 'Building ZFS on a Pine64 Rock64 Ubuntu Distro' },
			{ hid: 'keywords', name: 'keywords', content: 'zfs,rock64,openzfs,zfsonlinx,ubuntu' },
		],
	},
}
</script>
