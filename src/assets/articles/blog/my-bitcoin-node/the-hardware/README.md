**Bitcoin Node: The Hardware**
# GIGABYTE Ultra Compact Mini PC - GB-BLCE-4105

---

I'm always looking for a compact, inexpensive, and performant computer capable of running a typical Bitcoin stack.
It can allow people to more easily leverage the power of Bitcoin.

Recently, I've been working with the <a v-external href="https://amzn.to/2X0olsk">GIGABYTE Ultra Compact Mini PC - GB-BLCE-4105</a>, an affordable "mini pc."

<app-img data-src="/img/photos/my-bitcoin-node/overview.jpg"></app-img>

The GB-BLCE-4105 is just the base computer, so additional components are required to get up and running.
Taking a look at the <a v-external href="https://www.gigabyte.com/Mini-PcBarebone/GB-BLCE-4105-rev-10#sp">spec page</a>, we can see that the memory is 2400MHz DDR4, and that there is support for both an M.2 SSD as well as a 2.5" SSD.
Here is the list of the additional components I am using:
* Memory (RAM): <a v-external href="https://amzn.to/2OifoGL">Crucial 8GB Single DDR4</a>
* Primary Storage: <a v-external href="https://amzn.to/2ujNaCp">Sabrent 256GB NVMe PCIe M.2 2280 SSD</a>
* Secondary Storage: <a v-external href="https://amzn.to/2UWUL4W">Silicon Power 1TB 2.5" SSD</a>

**Note:**
The *primary storage* is the drive that I use to install and run the Operating System.
The *secondary storage* is the drive that I use to store the blockchain data.

The case can be opened by removing the four screws on the bottom and then removing the bottom.
Here is what it looks like opened up with all of the slots that we'll be using labeled:

<app-img data-src="/img/photos/my-bitcoin-node/overview-open-labeled.jpg"></app-img>

The total cost came to about 350 USD.
The rest of this article will show how I assembled everything.
(The order does not matter.)

<app-img data-src="/img/photos/my-bitcoin-node/installing-secondary-storage.jpg"></app-img>

---

## Installing the Memory (RAM)

Perhaps the easiest step is to install our single 8GB stick of DDR4 RAM.
As you can see in the photo below, it initially sits in the slot loosly and at an angle.
Then, by pressing down, it goes below the metal clips and snaps into place.

<app-img data-src="/img/photos/my-bitcoin-node/installing-ram.jpg"></app-img>

## Installing the 2.5" SATA III SSD

The inside of the bottom part is actually a cage that will hold the 2.5" SSD.
As seen in the photo below, the SSD should be mounted with the SATA port facing the open side of the cage.
The GB-BLCE-4105 comes with four screws for attaching the SSD to the cage.

<app-img data-src="/img/photos/my-bitcoin-node/installing-secondary-storage.jpg"></app-img>

## Installing the M.2 NVMe PCIe SSD

Finally, install the primary storage, the NVMe PCIe SSD.
The M.2 slot is directly above the wireless card.
Simply remove the screw, seat the drive in the slot, and then push down and put the screw back in order to hold the drive in place.

<app-img data-src="/img/photos/my-bitcoin-node/installing-primary-storage.jpg"></app-img>

## Conclusion

Now that everything is installed, attach the bottom of the case back with the four screws, and we're good to go!
The BIOS can be accessed by pressing the DELETE key on boot.

As far as operating systems, Linux Mint and Manjaro worked perfectly fine out of the box.
I am currently running FreeBSD, but there were a few speed bumps along the way.
In another article, I'll detail my FreeBSD setup.

<script>
export default {
	head: {
		title: 'GIGABYTE Ultra Compact Mini PC - GB-BLCE-4105',
		meta: [
			{ hid: 'description', name: 'description', content: 'Assembling a GIGABYTE GB-BLCE-4105, RAM, and storage.' },
			{ hid: 'keywords', name: 'keywords', content: 'gigabyte,gb-blce-4105,ram,memory,ssd,storage' },
		],
	},
}
</script>
