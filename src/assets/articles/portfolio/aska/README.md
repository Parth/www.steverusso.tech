# ![](https://www.allstarkidsnj.com/favicon.ico) All Star Kids Academy

<a v-external href="https://www.allstarkidsnj.com/">All Star Kids Academy</a> is a full service, state licensed daycare located in Ringwood, NJ.
Our work originally was a single page website along with cleaning up the search results pages.
This included adding missing information or correcting existing information.

<app-img data-src="/img/portfolio/aska-01.png"></app-img>

However, an interesting addition to website came when the owners wanted to allow users to schedule a tour of the daycare by filling out a small form online.
We decided to do everything from scratch instead of using a plugin service.

The conditions were as follows.
* Tours can only be scheduled during weekdays and no more than 30 days in advance.
* Three 'time slots' are made available:
	1. morning (9am - 11:30am)
	2. early afternoon (2pm - 4pm)
	3. late afternoon (4pm - 6pm)
* Only one tour can be booked per time slot.

<app-img data-src="/img/portfolio/aska-schedule-tour.png"></app-img>

With those in mind, the goal was to:

1. Gather information from the user such as...
	* Basic contact info (name, email, phone)
	* A date and time slot (morning, early afternoon, or late afternoon)
	* Spam prevention measure (Google recaptcha in this case)
2. Send the information to our server to...
	* Verify the spam prevention measure
	* Email both the user and the day care owners with the tour information
	* Store the information in a database

Both the <a v-external href="https://gitlab.com/steverussotech/allstarkidsnj.com">website (front end)</a> and <a v-external href="https://gitlab.com/steverussotech/allstarkidsnj.com-server">API (back end)</a> are open source.
(Documentation and development instructions can be found in each project's repo.)

While the front end was simple enough, the server was full of interesting challenges.
Written in Go, it covers some cool things such as using Google's recaptcha API for spam prevention, using Mailgun's API for sending out the notification emails, and using SQLite for storing the tour appointment info.

<script>
export default {
	head: {
		title: 'Portfolio - All Star Kids Academy',
	},
}
</script>
