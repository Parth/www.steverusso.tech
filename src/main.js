import Vue from 'vue'
import VueMeta from 'vue-meta'
import VueRouter from 'vue-router'

// Plugins.
require('./plugins/directives')
require('./plugins/highlightjs')
require('./plugins/vuetify')

import 'vuetify/src/stylus/app.styl'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'highlight.js/styles/atom-one-dark-reasonable.css'

// Globally register certain custom components.
Vue.component('app-container',    () => import('@/components/AppContainer.vue'))
Vue.component('app-img',          () => import('@/components/AppImg.vue'))
Vue.component('app-info-piece',   () => import('@/components/AppInfoPiece.vue'))
Vue.component('app-info-section', () => import('@/components/AppInfoSection.vue'))

import App from './App.vue'

// Router.
const router = new VueRouter({
	mode: 'history',
	routes: require('./routes').routes,
})

// Vue.use()
Vue.use(VueMeta, { keyName: 'head' })
Vue.use(VueRouter)

// Vue.config
Vue.config.productionTip = false

// Vue instance.
new Vue({
	render: function (h) { return h(App) },
	router,
}).$mount('#app')
