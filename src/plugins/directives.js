import Vue from 'vue'

// The 'external' directive is used for applying the 'target' and 'rel'
// attributes to link elements (<a></a>) that are meant to open in a new tab.
Vue.directive('external', {
	inserted: function (el) {
		el.setAttribute('target', '_blank')
		el.setAttribute('rel', 'noopener noreferrer')
	},
})

// The 'wh' directive is used for setting the 'width' and 'height'
// attributes of img elements.
Vue.directive('wh', {
	bind: function (el, binding) {
		el.setAttribute('width', binding.value)
		el.setAttribute('height', binding.value)
	},
})
