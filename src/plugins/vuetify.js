import Vue from 'vue'

import Vuetify, {
	VApp,
	VToolbar, VToolbarTitle, VToolbarItems, VToolbarSideIcon,
	VNavigationDrawer,
	VContent,
	VFadeTransition,
	VContainer, VLayout, VFlex,
	VCard, VCardTitle, VCardText, VCardActions,
	VSpacer, VDivider,
	VTabs, VTab, VTabItem,
	VBtn,
	VDialog,
	VIcon,
	VImg,
} from 'vuetify/lib'
import { Ripple } from 'vuetify/lib/directives'

Vue.use(Vuetify, {
	components: {
		VApp,
		VToolbar, VToolbarTitle, VToolbarItems, VToolbarSideIcon,
		VNavigationDrawer,
		VContent,
		VFadeTransition,
		VContainer, VLayout, VFlex,
		VCard, VCardTitle, VCardText, VCardActions,
		VSpacer, VDivider,
		VTabs, VTab, VTabItem,
		VBtn,
		VDialog,
		VIcon,
		VImg,
	},
	directives: {
		Ripple,
	},
})
