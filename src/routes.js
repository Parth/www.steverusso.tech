import ViewArticle from '@/views/ViewArticle.vue'
import ViewHome from '@/views/ViewHome.vue'
import ViewBlog from '@/views/ViewBlog.vue'
import ViewPortfolio from '@/views/ViewPortfolio.vue'
import ViewBitcoin from '@/views/ViewBitcoin.vue'

// Application routes.
export const routes = [
	{ path: '/', component: ViewHome },
	{ path: '/blog/', component: ViewBlog },
	{ path: '/blog/:path*', component: ViewArticle },
	{ path: '/portfolio/', component: ViewPortfolio },
	{ path: '/portfolio/:path*', component: ViewArticle },
	{ path: '/bitcoin/', component: ViewBitcoin },
]
