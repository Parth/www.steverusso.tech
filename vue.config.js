
let plugins = []

// If the 'ANALYZE' environment variable is set to
// true, then use the webpack-bundle-analyzer plugin.
if (process.env.ANALYZE === 'true') {
	const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
	plugins.push(new BundleAnalyzerPlugin())
}

module.exports = {
	configureWebpack: {
		plugins: plugins,
	},
	chainWebpack: config => {
		config.module
			.rule('md')
			.test(/\.md/)
			.use('vue-loader')
			.loader('vue-loader')
			.end()
			.use('vue-markdown-loader')
			.loader('vue-markdown-loader/lib/markdown-compiler')
			.options({
				raw: true,
			})
	},
}
